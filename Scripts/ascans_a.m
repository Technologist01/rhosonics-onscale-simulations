close all
clear all
clc

folder = 'E:\Rhosonics\Simulations\AScan\Combined AScans';

cd(folder);

files = dir('*.flxhst');


l = length(files);

for i =1:l
  Data = read_history(files(i).name);
  ##Data = read_history('SDM01.flxhst');
  
  time = Data.TimeRecB.Time*1e6;
  v(:,:,i) = [Data.DataRecB(:).Data];
  x = [Data.DataRecB(:).DataMax];
  y = [Data.DataRecB(:).DataMin];
  w(:,:,i) = x - y;
  
  XYZ(:,:,i) = [Data.DataRecB(:).XYZ]*1e3;
  
end

figure
plot(time,v(:,2,:),'LineWidth', 2)
box off
xlabel('Time (\mus)')
ylabel('Voltage (V)')
title('AScans')
ylim([-0.05, 0.05])
h = legend('Full', "Bull's Eye");
set(h, 'title', 'Electrode Pattern')
legend boxoff
saveas(gcf, 'AScans','png')

xlim([5, 20])
saveas(gcf, 'AScans-zoomed','png')

T = indeks(time,[5,8]);
B = windw(squeeze(v(:,2,:)),T);

[V, f] = freq_profile(B,1024,1/time(1));
figure
plot(f, abs(V), 'LineWidth', 2)
xlabel('Frequency (MHz)')
ylabel('Voltage (V)')
title('First Reflection Frequency Profile')
box off
h = legend('Full', "Bull's Eye");
set(h, 'title', 'Electrode Pattern')
legend boxoff
xlim([0, 8])
saveas(gcf, 'Frequency Profile','png')

figure
plot(f, abs(V)./max(abs(V)), 'LineWidth', 2)
xlabel('Frequency (MHz)')
ylabel('Voltage (V)')
title('Normalised First Reflection Frequency Profile')
box off
h = legend('Full', "Bull's Eye");
set(h, 'title', 'Electrode Pattern')
legend boxoff
xlim([0, 6])
saveas(gcf, 'Normalised Frequency Profile','png')