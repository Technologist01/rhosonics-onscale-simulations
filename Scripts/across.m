close all
clear all
clc

folder = 'E:\Rhosonics\Simulations\Beam\60mm combined';

cd(folder);

files = dir('*.flxhst');


l = length(files);

for i =1:l
  Data = read_history(files(i).name);
  ##Data = read_history('SDM01.flxhst');
  
  time = Data.TimeRecB.Time*1e6;
  v(:,:,i) = [Data.DataRecB(:).Data];
  x = [Data.DataRecB(:).DataMax];
  y = [Data.DataRecB(:).DataMin];
  w(:,:,i) = x - y;
  
  XYZ(:,:,i) = [Data.DataRecB(:).XYZ]*1e3;
  
end

figure
plot(time,v(:,4:end))
box off
xlabel('Time (\mus)')

Press_line = [fliplr(w(1,5:end,:)),w(1,4:end,:)];
Press_line = squeeze(Press_line);
x_rang = [fliplr(-XYZ(1,5:end,:)),XYZ(1,4:end,:)];
x_rang = squeeze(x_rang);
x = -10:0.1:10;
Press = interp1(x_rang(:,1), Press_line, x, 'spline','extrap');

figure
plot(x,Press/maxim(Press), 'LineWidth', 2)
xlabel('Position (mm)')
ylabel('Normalised Pressure')
title('Pressure Profile 20mm from piezo')
box off
h = legend('Full', "Bull's Eye");
set(h, 'title', 'Electrode Pattern')
legend boxoff
grid
saveas(gcf, 'Line Pressure', 'png')
